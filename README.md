# kvm-lab-automation

Ansible playbooks to create a simple kvm based env on the local machine. Its using cloud images based on either cloud-init or ignition for the basic access config.


## Install ansible

Using built-in ansible on fedora 33 for testing. python3-pycdlib is needed for iso creation.

```
$ sudo dnf -y install ansible python3-pycdlib python3-netaddr

$ ansible --version
ansible 2.9.15
  ...
  python version = 3.9.0 (default, Oct  6 2020, 00:00:00) [GCC 10.2.1 20200826 (Red Hat 10.2.1-3)]

```

## Install any ansible collections and roles

```
$ ansible-galaxy collection install -r ansible-requirements.yml
```

## Install libvirt

```
$ ansible-playbook install_libvirt.yaml
```

## Configure the env

Examples can be found in vars/

## Build the env

```
ansible-playbook build_env.yml -e @./vars/centos7-kube.yaml
```

## Install kubernetes onto the nodes

```
ansible-playbook install_kubernetes.yml -e @./vars/centos7-kube.yaml
```

## Destroy the env

```
ansible-playbook destroy_env.yml -e @./vars/centos7-kube.yaml
```

## ESXi related env notes

```
# esxi build example
ansible-playbook build_env.yml -e @./vars/esxi.yaml

# esxi destroy example
ansible-playbook destroy_env.yml -e @./vars/esxi.yaml

# The destory doesn't remove the base iso and image.
# The keys are also not destroyed as the base image uses them. 
```


